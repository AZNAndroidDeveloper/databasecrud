package uz.azn.workedwithdatabase.recycleViewAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text
import uz.azn.workedwithdatabase.Person
import uz.azn.workedwithdatabase.R

class RecyclerViewAdapter(var listOfItems:MutableList<Person>): RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    lateinit var listener:OnItemClickListener

   var personList:MutableList<Person> = arrayListOf()
    init {
        this.personList = listOfItems
    }
    inner class ViewHolder(itemview: View):RecyclerView.ViewHolder(itemview){
        val firstName = itemview.findViewById<TextView>(R.id.tv_first)
        val lastName = itemview.findViewById<TextView>(R.id.tv_last)
        val age =itemview.findViewById<TextView>(R.id.tv_age)


        fun bind(person: Person){
            firstName.text = person.fName
            lastName.text = person.lName
            age.text = person.age.toString()

        }
        init {
           itemview.setOnLongClickListener {
               if (adapterPosition !=RecyclerView.NO_POSITION)
                   listener.onItemLongClicked(adapterPosition)
               true
           }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_user,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listOfItems.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(personList[position])


    }
    fun setOnItemLongClickListener(listener:OnItemClickListener){
        this.listener = listener
    }

    interface OnItemClickListener{
        fun onItemLongClicked(position: Int)
    }
}