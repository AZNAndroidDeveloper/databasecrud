package uz.azn.workedwithdatabase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import uz.azn.workedwithdatabase.databinding.ActivityMainBinding
import uz.azn.workedwithdatabase.introFragment.IntroFragment

class MainActivity : AppCompatActivity() {
    val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val manager = supportFragmentManager
        manager.beginTransaction().replace(binding.frameLayout.id,IntroFragment(this)).commit()
    }
}