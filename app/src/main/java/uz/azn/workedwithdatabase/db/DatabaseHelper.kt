package uz.azn.workedwithdatabase.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import android.provider.BaseColumns._ID

val DATABASE_NAME = "user_database"
val DATABE_VERSION = 1
@Suppress("EqualsBetweenInconvertibleTypes")
class DatabaseHelper(context: Context):SQLiteOpenHelper(context, DATABASE_NAME,null, DATABE_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createTable = "CREATE TABLE ${DatabaseContainer.TABLE_NAME}(${BaseColumns._ID} INTEGER PRIMARY KEY AUTOINCREMENT," +
                "${DatabaseContainer.FIRST_NAME_COL}  TEXT, ${DatabaseContainer.LAST_NAME_COL} TEXT," +
                "${DatabaseContainer.AGE} INTEGER)"
   db!!.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS ${DatabaseContainer.TABLE_NAME}")
    }

    fun insertData(fName:String,lName:String, age :Int):Boolean{
        val db:SQLiteDatabase = this.writableDatabase
        val contentValues = ContentValues()
        with(contentValues){
            put(DatabaseContainer.FIRST_NAME_COL,fName)
            put(DatabaseContainer.LAST_NAME_COL,lName)
            put(DatabaseContainer.AGE,age)

        }
        val isInsert = db.insert(DatabaseContainer.TABLE_NAME,null,contentValues)
        db.close()
        return !isInsert.equals(-1)
    }

    fun readData():Cursor{
        val db = this.writableDatabase
        val read = db.rawQuery("SELECT * FROM ${DatabaseContainer.TABLE_NAME}", null)
        return read
    }
    fun deleteData(id:String):Boolean{
        val db = this.writableDatabase
        val isDeleteData = db.delete(DatabaseContainer.TABLE_NAME,"$_ID=?", arrayOf(id))
        return isDeleteData != -1
    }
    fun updateData(id:String,fName:String, lName:String, age:Int):Boolean{
        val dp = this.writableDatabase
        val contentValues = ContentValues()
        with(contentValues){
            put(DatabaseContainer.FIRST_NAME_COL,fName)
            put(DatabaseContainer.LAST_NAME_COL,lName)
            put(DatabaseContainer.AGE,age)

        }
       val isUpdateData= dp.update(DatabaseContainer.TABLE_NAME,contentValues,"$_ID=?", arrayOf(id))
        return isUpdateData != -1
    }

}