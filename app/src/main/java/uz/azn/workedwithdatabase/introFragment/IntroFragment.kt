package uz.azn.workedwithdatabase.introFragment

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import org.w3c.dom.Text
import uz.azn.workedwithdatabase.Person
import uz.azn.workedwithdatabase.R
import uz.azn.workedwithdatabase.R.id.*
import uz.azn.workedwithdatabase.databinding.FragmentIntroBinding
import uz.azn.workedwithdatabase.db.DatabaseHelper
import uz.azn.workedwithdatabase.recycleViewAdapter.RecyclerViewAdapter

class IntroFragment(var mContext: Context?) : Fragment(R.layout.fragment_intro) {
    private lateinit var binding: FragmentIntroBinding
    var list: MutableList<Person> = ArrayList()
    private lateinit var databaseHelper: DatabaseHelper
    private lateinit var recycleAdapter: RecyclerViewAdapter

    init {

        mContext = context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        databaseHelper = DatabaseHelper(requireContext())
        initializeList()
        recycleAdapter = RecyclerViewAdapter(list)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, true)
        layoutManager.stackFromEnd = true
        binding.recycleView.layoutManager = layoutManager
        binding.recycleView.adapter = recycleAdapter
        binding.recycleView.addItemDecoration(
            DividerItemDecoration(
                binding.recycleView.context,
                DividerItemDecoration.VERTICAL
            )
        )
        binding.btnSave.setOnClickListener {
            insertData()
        }
        recycleAdapter.setOnItemLongClickListener(object : RecyclerViewAdapter.OnItemClickListener {
            override fun onItemLongClicked(position: Int) {
                showUpdate(position)


            }

        })
    }

    private fun showUpdate(position: Int) {
        val builder = AlertDialog.Builder(requireContext())
        val view = activity!!.layoutInflater.inflate(R.layout.custom_alert, null, false)
        var firstName = view.findViewById<EditText>(et_FirstName)
        var lastName = view.findViewById<EditText>(et_LastName)
        var age = view.findViewById<EditText>(etAge)
        var buttonUpdate = view.findViewById<Button>(update)
        var buttonDelete = view.findViewById<Button>(delete)

        builder.setView(view)
        firstName.setText(list[position].fName)
        lastName.setText(list[position].lName)
        age.setText(list[position].age.toString())

        val dialog = builder.create()
        dialog.show()
        buttonUpdate.setOnClickListener {
            updateData(
                list[position].id,
                firstName.text.toString(),
                lastName.text.toString(),
                age.text.toString().toInt()
            )
            dialog.dismiss()
        }
        buttonDelete.setOnClickListener {
            deleteData(list[position].id)
            dialog.dismiss()
        }

    }

    fun insertData() {
        if (binding.etFName.text.isNotEmpty() && binding.etLName.text.isNotEmpty() && binding.etAge.text.isNotEmpty()) {
            val fName = binding.etFName.text.toString()
            val lName = binding.etLName.text.toString()
            val age = binding.etAge.text.toString().toInt()
            databaseHelper.insertData(fName, lName, age)
            list.clear()
            initializeList()
            recycleAdapter.notifyDataSetChanged()
        } else
            Toast.makeText(requireContext(), "Please fill up the fields ", Toast.LENGTH_SHORT)
                .show()
    }

    fun initializeList() {
        val cursor = databaseHelper.readData()
        if (cursor.count > 0) {
            while (cursor.moveToNext()) {
                val id = cursor.getString(0)
                val fName = cursor.getString(1)
                val lName = cursor.getString(2)
                val age = cursor.getInt(3)
                val person = Person(id, fName, lName, age)
                list.add(person)

            }
        }
    }

    fun updateData(position: String, firstName: String, lastName: String, age: Int) {
        databaseHelper.updateData(position, firstName, lastName, age)
        list.clear()
        initializeList()
        recycleAdapter.notifyDataSetChanged()
    }

    fun deleteData(position: String) {
        val deleteid = position
        databaseHelper.deleteData(deleteid)
        list.clear()
        initializeList()
        recycleAdapter.notifyDataSetChanged()
    }
}